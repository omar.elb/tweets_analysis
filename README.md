# Overview:

This project is an implementation of a Tweets analysis solution.
It provides an up-to-date status of tweets with the hashtag #Arcane by requesting Data from Twitter API and storing it in GCP Bigquery.

## Components:

This solution can be used as a web server to request stored data in a (very) basic interface. The data that is available is fetched and updated asynchronously.
A scheduler triggers the flow of querying the remote API.
Database updates are not handled by the scheduler. The scheduler posts a message for each tweet update into a RabbitMQ Queue to decouple the fetch and update streams.
A thread of the main application consumes this queue (could be separated to another service) and sends requests to a google cloud function that handles write operations into the Database.

## Setup:

### Prepare environment:
* install gcloud CLI: https://cloud.google.com/sdk/docs/install
* install python3 and prepare virtual environment:
    * `python3 -m venv env`
    * `source env/bin/activate`
* install requirements:
    * `python3 -m pip install -r requirements`
* ask project owner for `.env.dev` file and some variables to add to `app.yaml`

### Usage:
* Launch server: `python3 main.py`
