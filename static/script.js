'use strict';
const rowsPerPage = 10;

window.addEventListener('load', function () {
    getTweets(0);

    $(document).on('click', "*[id*='page']", function () {
        let pageNb = this.id.split('=')[1];
        getTweets(pageNb - 1);
    });
});

function getTweets(pageNb) {
    fetch(`/tweets/${pageNb}`)
    .then(function (response) {
        return response.json();
    }).then(function (data) {
        console.log('GET response:');
        if (data.tweets && data.tweets.length > 0) {
            fillTweetsTable(data);
        }
    });
}

function fillTweetsTable(data) {
    $("#tweetsTable").empty();
    $("#tweetsPagination").empty();
    const { tweets, totalCount } = data;
    const pagesNumber = Math.ceil(totalCount / rowsPerPage);
    const headers = Object.keys(tweets[0]);
    const tmplData = { data: tweets, headers };
    const paginationTmplData = { pages: Array(pagesNumber).fill(0).map((e, i) => i + 1) };
    $("#tweetsTableTemplate").tmpl(tmplData).appendTo("#tweetsTable");
    $("#paginationTemplate").tmpl(paginationTmplData).appendTo("#tweetsPagination");
}
