from os import environ
from os.path import exists
import json
import threading
import time
from flask import Flask, render_template, jsonify
from modules import tweets, scheduler, consumer
import functions_framework

app = Flask(__name__)

def handle_background_tasks():
    scheduler.launch_tweet_ingestion_if_necessary()

def start_app():
    app.run(host='127.0.0.1', port=8080)

def handle_env_variables():
    if exists('.env.dev'):
        with open('.env.dev') as f:
            content = f.read().split('\n')
            for line in content:
                key, value = line.split('=', 1)
                environ[key] = value

@functions_framework.http
def tweet_update_worker(request):
    tweets.update_tweets_database(json.loads(request.data))
    headers = {
        'Access-Control-Allow-Origin': '*'
    }
    return (jsonify({ "success": True }), 200, headers)

@app.route('/')
def route():
    return render_template('index.html')

@app.route('/tweets/<page_no>', methods=["GET"])
def get_tweets(page_no=0):
    page_no = int(page_no)
    page_offset = 10
    return jsonify(tweets.get_tweets(limit=page_offset, offset=page_no * page_offset))

def check_period_events(period=5):
    while True:
        time.sleep(period)
        handle_background_tasks()

def main():
    handle_env_variables()
    th = threading.Thread(target=check_period_events)
    th.start()
    consumer.consume_tweet_queue()
    start_app()
    th.join()


if __name__ == '__main__':
    main()
