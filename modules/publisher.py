import os
from aio_pika import Message, connect

class RabbitPublisher:
    def __init__(self, queue_name="tweet_handler") -> None:
        self.queue_name = queue_name
        self.url = os.environ.get('CLOUDAMQP_URL')
    
    async def publish(self, message: str) -> None:
        connection = await connect(self.url)

        async with connection:
            channel = await connection.channel()
            queue = await channel.declare_queue(self.queue_name)
            await channel.default_exchange.publish(
                Message(bytes(message, 'utf-16')),
                routing_key=queue.name,
            )
            print(f"Sent message: {message}'")

