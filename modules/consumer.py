import os
import aiohttp
import asyncio
from threading import Thread
from modules import tweets

import json

from aio_pika import connect
from aio_pika.abc import AbstractIncomingMessage


async def call_worker(message: AbstractIncomingMessage):
    async with aiohttp.ClientSession() as session:
        decoded = message.body.decode('utf-16')
        url = os.environ.get('TWEET_MESSAGE_WORKER')
        resp = await session.post(url, data=decoded)
        print(resp)
        # print(f'{decoded}')
        # tweets.update_tweets_database(json.loads(decoded))


class RabbitConsumer:
    def __init__(self, queue_name="tweet_handler") -> None:
        self.queue_name = queue_name
        self.url = os.environ.get('CLOUDAMQP_URL')

    async def consume(self, callback):
        connection = await connect(self.url)
        async with connection:
            channel = await connection.channel()
            queue = await channel.declare_queue(self.queue_name)
            await queue.consume(callback, no_ack=True)

            print(f"Consuming Queue {self.queue_name}")
            await asyncio.Future()

def init_consumer():
    print(f'Launch rabbit process consumer')
    consumer = RabbitConsumer()
    asyncio.run(consumer.consume(call_worker))

def consume_tweet_queue():
    th = Thread(target=init_consumer)
    th.start()
