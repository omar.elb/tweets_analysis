import re
from datetime import datetime, timedelta
from google.cloud import bigquery

client = bigquery.Client()

global count_memory
count_memory = None

def get_tweets(limit, offset):
    global count_memory
    result = []
    query = f"""SELECT * FROM arcane_hashtag_tweets.tweets LIMIT {limit} OFFSET {offset}"""
    if count_memory is None:
        count_query = """SELECT COUNT(*) FROM arcane_hashtag_tweets.tweets"""
        count_cursor = client.query(count_query).result()
        for x in count_cursor:
            count_memory = x[0]
    tweets_cursor = client.query(query).result()
    for row in tweets_cursor:
        result.append({
            'id': row.get("externalId", ""),
            'content': row.get("content", ""),
            'likes': row.get("likes", ""),
            'retweets': row.get("retweets", ""),
            'replies': row.get("replies", ""),
            'video_views': row.get("video_views", ""),
        })
    return {
        "totalCount": count_memory,
        "tweets": result
    }

def update_tweets_database(tweet):
    stream_buffer_datetime = (datetime.utcnow() - timedelta(minutes=100)).strftime('%Y-%m-%d %H:%M:%S')
    query = f"""SELECT * FROM arcane_hashtag_tweets.tweets WHERE externalId = '{tweet["id"]}' AND last_update > '{stream_buffer_datetime}'"""
    tweet_cursor = client.query(query).result()
    if tweet_cursor.total_rows == 0:
        tweet["externalId"] = tweet['id']
        tweet["last_update"] = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        del tweet['id']
        rows_to_insert = [ tweet ]
        dataset = client.dataset("arcane_hashtag_tweets")
        table = client.get_table(dataset.table('tweets'))
        errors = client.insert_rows_json(table, rows_to_insert, [None] * len(rows_to_insert))
        if errors != []:
            print(f'Errors on tweets insert {errors}')
    else:
        query = """UPDATE arcane_hashtag_tweets.tweets SET content='''{}''', likes={},""" +\
            """ retweets={}, replies={},""" +\
                 """ video_views={} WHERE externalId='''{}''' """
        query = query.format(
                        re.sub(r',|"|\'', '', re.sub(r' |\n|\t|\r|\s',' ',tweet["content"])),
                        tweet['likes'],
                        tweet['retweets'],
                        tweet['replies'],
                        tweet['video_views'],
                        tweet["id"])
        print(f'==============> Updating query={query}')
        res = client.query(query).result()
        for x in res:
            print(x)
