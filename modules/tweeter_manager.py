import os
import requests
import json
import asyncio
from modules import publisher

def look_for_videa_count(attachments, media_mapping):
    for key in attachments.get("media_keys", []):
        if key in media_mapping and media_mapping[key]:
            return media_mapping[key]
    return 0

def fetch_search_data(next_page_token = None):
    endpoint = os.environ['SEARCHTWEETS_ENDPOINT']
    bearer = os.environ['SEARCHTWEETS_BEARER_TOKEN']
    url = f'{endpoint}?query=%23Arcane&max_results=10&tweet.fields=public_metrics,text,id&expansions=attachments.media_keys&media.fields=public_metrics'
    if next_page_token:
        url += f'&pagination_token={next_page_token}'
    headers = {
        "Authorization": f'Bearer {bearer}'
    }
    response = requests.get(url=url, headers=headers)
    data = json.loads(response.content)
    next_token = data["meta"].get("next_token", None) if "meta" in data and data["meta"] else None
    sender = publisher.RabbitPublisher()
    media_mapping = {}
    if "includes" in data and data["includes"] and "media" in data["includes"]:
        for e in data["includes"].get("media", []):
            if "media_key" in e and "public_metrics" in e and "type" in e and e["type"] == "video":
                media_mapping[e["media_key"]] = e["public_metrics"].get("view_count", None)

    for elem in data['data']:
        asyncio.run(sender.publish(json.dumps({
            "id": elem["id"],
            "content": elem["text"],
            "likes": elem["public_metrics"]["like_count"],
            "retweets": elem["public_metrics"]["retweet_count"],
            "replies": elem["public_metrics"]["reply_count"],
            "video_views": look_for_videa_count(elem["attachments"], media_mapping)\
                 if "attachments" in elem and elem["attachments"] else 0,
        })))
    if next_token:
        fetch_search_data(next_token)
