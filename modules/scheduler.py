from curses import raw
from datetime import datetime, timedelta
from google.cloud import bigquery
from modules import tweeter_manager

client = bigquery.Client()

JOB_LAUNCHED_RECENTLY = False

def perform_massive_ingestion() -> None:
    tweeter_manager.fetch_search_data()

def launch_tweet_ingestion_if_necessary():
    global JOB_LAUNCHED_RECENTLY
    print('===', datetime.utcnow().hour)
    if 19 <= datetime.utcnow().hour < 21 and not JOB_LAUNCHED_RECENTLY:
        JOB_LAUNCHED_RECENTLY = True
        perform_massive_ingestion()
    elif datetime.utcnow().hour > 1 and JOB_LAUNCHED_RECENTLY:
        JOB_LAUNCHED_RECENTLY = False
